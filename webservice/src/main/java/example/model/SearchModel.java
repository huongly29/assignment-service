package example.model;

import example.entity.Place;
import example.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class SearchModel {
    Transaction transaction = null;

    public List<Place> searchByName(String placeName){
        try (Session session = HibernateUtil.getSession()){
            Query query = session.createQuery("FROM Place where name like concat('%',:placeName,'%') ");
            query.setParameter("placeName", placeName);
            return (List<Place>) query.list();
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
