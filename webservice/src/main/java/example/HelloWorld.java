package example;

import com.google.gson.Gson;
import example.entity.*;
import example.model.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.List;

@WebService()
public class HelloWorld {
    private MemberModel memberModel = new MemberModel();
    private DistrictModel districtModel = new DistrictModel();
    private CityModel cityModel = new CityModel();
    private PlaceModel placeModel = new PlaceModel();
    private RatingModel ratingModel = new RatingModel();
    private CommentModel commentModel = new CommentModel();
    private SearchModel searchModel = new SearchModel();

    /**
     * Start City Area
     * */
    @WebMethod
    public List<City> getAllCity(){
        return cityModel.getAll();
    }

    @WebMethod
    public City getOneCity(long id){
        return cityModel.getById(id);
    }

    /**
     * End City Area
     * */

    /**
     * Start District Area
     * */
    @WebMethod
    public List<District> getAllDistrict(){
        return districtModel.getAll();
    }

    @WebMethod
    public District getOneDistrict(long id){
        return districtModel.getById(id);
    }
    /**
     * End District Area
     * */

    /**
     * Start Place Area
     * */
    @WebMethod
    public List<Place> getAllPlace() {
        List<Place> list = new ArrayList<Place>();
        PlaceModel placeModel = new PlaceModel();
        list = placeModel.getAll();
        return list;
    }

    @WebMethod
    public Place getDetailPlace(long id) {
        Place place = placeModel.getById(id);
        return place;
    }

    @WebMethod
    public Place createPlace(Place cPlace) {
        Place place = new Place();
        place.setName(cPlace.getName());
        place.setCity(cPlace.getCity());
        place.setDescription(cPlace.getDescription());
        place.setDistrict(cPlace.getDistrict());
        place.setMember(cPlace.getMember());
        place.setImages(cPlace.getImages());
        Place result = placeModel.store(place);
        return result;
    }

    /**
     * End Place Area
     * */

    /**
     * Start Rating Area
     * */
    @WebMethod
    public Rating createRating(Rating cRating) {
        Rating rating = new Rating();
        rating.setValue(cRating.getValue());
        rating.setMember(cRating.getMember());
        Rating result = ratingModel.store(rating);
        return result;
    }

    public List<Rating> getRatingByPlaceId(long placeId){
        return ratingModel.getByPlaceId(placeId);
    }
    /**
     * End Rating Area
     * */

    /**
     * Start Comment Area
     * */
    @WebMethod
    public Comment createComment(Comment cComment) {
        Comment comment = new Comment();
        comment.setComment(cComment.getComment());
        comment.setPlace(cComment.getPlace());
        comment.setMember(cComment.getMember());
        Comment result = commentModel.store(comment);
        return result;
    }

    public List<Comment> getCommentByPlaceId(long placeId){
        return commentModel.getByPlaceId(placeId);
    }

    /**
     * End Comment Area
     * */

    /**
     * Start Member Area
     * */
    @WebMethod
    public Member register(Member cMember) {
        Member member = new Member();
        member.setName(cMember.getName());
        member.setPassword(cMember.getPassword());
        member.setUsername(cMember.getUsername());
        member.setRole(cMember.getRole());
        member.setGender(cMember.getGender());
        Member result = memberModel.register(member);
        return result;
    }

    @WebMethod
    public Member login(String usesrname, String password) {
        Member member = new Member();
        member = (Member)  memberModel.login(usesrname, password);
        return member;
    }

    /**
     * End Member Area
     * */

    /**
     * Start Search Area
     * */
    public List<Place> getPlaceByCityId(long cityId){
        return placeModel.getPlaceByCity(cityId);
    }

    public List<Place> getPlaceByDistrictId(long districtId){
        return placeModel.getPlaceByDistrict(districtId);
    }

    public List<Place> getPlaceByName(String name){
        return searchModel.searchByName(name);
    }
    /**
     * End Search Area
     * */
    @WebMethod
    public String sayHelloWorldFrom(String from) {
        String result = "Hello, world, from " + from;
        System.out.println(result);
        return result;
    }

    public static void main(String[] argv) {
        Object implementor = new HelloWorld();
        String address = "http://localhost:9000/HelloWorld";
        Endpoint.publish(address, implementor);

    }
}
