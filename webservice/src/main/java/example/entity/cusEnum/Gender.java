package example.entity.cusEnum;

public enum Gender {
    MALE(1), FEMALE(0), OTHERS(2);
    int value;

    Gender(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public Gender findByValue(int value){
        for (Gender gender: Gender.values()) {
            if (gender.getValue() == value){
                return gender;
            }
        }
        return null;
    }
}
