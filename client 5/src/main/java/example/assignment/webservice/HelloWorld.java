/**
 * HelloWorld.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package example.assignment.webservice;

public interface HelloWorld extends java.rmi.Remote {
    public example.assignment.webservice.Member register(example.assignment.webservice.Member arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.City getOneCity(long arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Member login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public example.assignment.webservice.Place[] getPlaceByCityId(long arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Place[] getPlaceByName(java.lang.String arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Place getDetailPlace(long arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Place createPlace(example.assignment.webservice.Place arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Rating createRating(example.assignment.webservice.Rating arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Place[] getAllPlace() throws java.rmi.RemoteException;
    public example.assignment.webservice.District[] getAllDistrict() throws java.rmi.RemoteException;
    public example.assignment.webservice.District getOneDistrict(long arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Comment createComment(example.assignment.webservice.Comment arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.City[] getAllCity() throws java.rmi.RemoteException;
    public example.assignment.webservice.Comment[] getCommentByPlaceId(long arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Rating[] getRatingByPlaceId(long arg0) throws java.rmi.RemoteException;
    public java.lang.String sayHelloWorldFrom(java.lang.String arg0) throws java.rmi.RemoteException;
    public example.assignment.webservice.Place[] getPlaceByDistrictId(long arg0) throws java.rmi.RemoteException;
}
