package example.assignment.controller;

import com.google.gson.Gson;
import example.assignment.service.CloudinaryService;
import example.assignment.webservice.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

@Controller
public class PostController {

    @Autowired
    HelloWorld helloWorld;

    @Autowired
    HttpSession session;

    @Autowired
    private CloudinaryService cloudinaryService;

    @RequestMapping(method = RequestMethod.GET, value = "/home")
    public String homePage(Model model) {
        try {
            System.out.println(new Gson().toJson(helloWorld.getAllPlace()));
            model.addAttribute("list", helloWorld.getAllPlace());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return "index";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/search")
    public String search(Model model, @RequestParam("search") String searchString) throws ServiceException, RemoteException {
        model.addAttribute("list", helloWorld.getPlaceByName(searchString));
        return "post/home";
    }

    @RequestMapping("/detail")
    public String getDetail() {
        return "post/detail";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/upload")
    public String createPlace(Model model) throws RemoteException {
        model.addAttribute("listCity", helloWorld.getAllCity());
        model.addAttribute("listDistrict", helloWorld.getAllDistrict());
        model.addAttribute("place", new Place());
        return "post/post";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/upload")
    public String storePost(@Valid Place place, BindingResult bindingResult, @RequestParam String cityId, @RequestParam String districtId, @RequestParam("file") MultipartFile file) {
        try {
//            if (bindingResult.hasErrors()) {
//                return "post/post";
//            }
            String url = cloudinaryService.uploadFile(file);
            City currentCity = helloWorld.getOneCity(Long.parseLong(cityId));
            District currentDistrict = helloWorld.getOneDistrict(Long.parseLong(districtId));
            Member currentMember = (Member) session.getAttribute("currentLoggedIn");
            place.setImages(url);
            place.setCity(currentCity);
            place.setDistrict(currentDistrict);
            place.setMember(currentMember);
            helloWorld.createPlace(place);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return "redirect:/home";
    }
}
