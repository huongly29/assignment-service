package example.assignment.controller;

import example.assignment.cusEnum.Role;
import example.assignment.cusEnum.Status;
import example.assignment.webservice.HelloWorld;
import example.assignment.webservice.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.rmi.RemoteException;

@Controller
public class MemberController {
    @Autowired
    HelloWorld helloWorld;

    @Autowired
    HttpSession session;

    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String login(Model model) {
        model.addAttribute("member", new Member());
        return "member/login";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/post")
    public String post(Model model) {
//        model.addAttribute("member", new Member());
        Member member = (Member) session.getAttribute("currentLoggedIn");
        if (member == null) {
            return "member/login";
        }
        return "post/post";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public String login(@Valid Member member, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "member/login";
        }
        try {
            Member currentMember = new Member();
            currentMember = (Member) helloWorld.login(member.getUsername(), member.getPassword());
            if (currentMember == null) {
                return "member/login";
            } else {
                if (currentMember.getStatus() == Status.ACTIVE.getValue()) {
                    if (currentMember.getRole() == Role.GUIDE.getValue()) {
                        session.setAttribute("currentLoggedIn", currentMember);
                        return "redirect:/upload";
                    } else if (currentMember.getRole() == Role.TRAVELER.getValue()) {
                        session.setAttribute("currentLoggedIn", currentMember);
                        return "index";
                    }
                } else {
                    return "member/login";
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            return "member/login";
        }
        return "member/login";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/register")
    public String createMember(Model model) {
        model.addAttribute("member", new Member());
        return "member/register";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    public String storeMember(@Valid Member member, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "member/register";
        }
        try {
            helloWorld.register(member);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return "post/home";
    }

}
