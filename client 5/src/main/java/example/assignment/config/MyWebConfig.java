package example.assignment.config;

import example.assignment.webservice.HelloWorld;
import example.assignment.webservice.HelloWorldServiceLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import javax.xml.rpc.ServiceException;

@Configuration
public class MyWebConfig {

    @Bean
    HelloWorld helloWorld() throws ServiceException {
        return new HelloWorldServiceLocator().getHelloWorldPort();
    }

}
